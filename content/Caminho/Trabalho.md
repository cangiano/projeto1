---
title: "Parte I - Professor Assistente por aí"
date: 2021-09-18T16:59:34-03:00
draft: false
---

### Resumo Básico e Palavras-Chave
- ***Empresa:*** Colégio Objetivo Mogi das Cruzes
- ***Função:*** Dar assistência aos alunos nos conteúdos de exatas e biológicas no período da tarde. Nesse ponto, a atividade geralmente é realizada individualmente, mas também eventualmente são realizadas aulas com um grupo maior.
- ***Período de trabalho:*** 2018-2020 e 2021-atualmente

- ***Competências:*** Comunicação Pessoal, Didática, Empatia, Criatividade e Domínio das disciplinas ensinadas.



### Breve descrição do período

No início de 2018, fui contratado no Colégio Objetivo de Mogi das Cruzes onde me formei em 2017.
As funções do cargo estão associadas ao auxílio dos alunos de cursinho e ensino médio no período 
da tarde (após as aulas da manhã) na consolidação dos aprendizados em sala, servindo como um recurso
adicional na aprendizagem apoiando explicações diferentes da teoria, resolvendo exercícios em conjunto
ou incentivando conversas sobre o futuro. Nesse contexto, atuei na tutoria de jovens em conteúdos
das áreas de exatas e biológicas, para temas associados ao vestibular e diversas olímpiadas do conhecimento.

Todos os plantonistas do colégio já passaram pelo mesmo caminho dos alunos e, de certa maneira, estão 
totalmente abertos a colaborar com o desenvolvimento dos outros jovens que querem passar em cursos
em grandes universidades.

É válido ressaltar que essa experiência profissional nos últimos 7 anos (com um hiato de 1 ano para
o desenvolvimento de uma pesquisa de iniciação científica com bolsa Fapesp entre o fim de 2020 e meados
do primeiro semestre de 2021) foi essencial no desenvolvimento das minhas capacidades de comunicação com
outras pessoas, dado que ensinar alguém exige muita paciência, clareza e consolidação do conteúdo de forma 
concisa. Portanto, mesmo que não seja a minha intenção seguir na carreira da educação, os aprendizados nesse
período continuarão sendo de suma importância no que se tornou o meu eu.
