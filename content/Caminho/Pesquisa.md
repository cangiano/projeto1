---
title: "Parte II - Ciências Moleculares? O que é isso?"
date: 2021-09-18T16:59:34-03:00
draft: false
---

### Pequeno Resumo do Período
- ***Graduação:*** Bacharelado em Ciências Moleculares
- ***Instituto:*** Pró reitoria da USP
- ***Período no Curso:*** 2018-2022
- ***Área de Pesquisa:*** Proteômica no contexto do Glioblastoma explorando bancos de dados públicos
- ***Competências desenvolvidas:*** Experimentos de rotina em laboratório (Cultura de células, PCr, western blot, etc), Proteômica, Transcriptômica, Python, R.

### Descrição mais detalhada do Período (2018-2022)

Transferi para o curso de Ciências Moleculares após o primeiro semestre de 2018. A ideia principal
era me dedicar a seguir um caminho na vida acadêmica de preferência em algum tópico relacionado a
Biologia.

O Curso tem como ideia principal capacitar os alunos a trabalharem em diversos tópicos dentro das 
áreas das exatas e biológicas, o que é feito com 2 anos iniciais de ciclo básico introduzindo
diversos tópicos e dando base a uma escolha de para onde seguir nos 2 anos seguintes. Assim, ao
se concluir o ciclo básico o aluno deveria ser capaz de escolher uma área de interesse e iniciar
uma pesquisa junto a um orientador, que contabiliza como uma disciplina de projeto na grade do curso.
Há muita flexibilidade em quais disciplinas cursar durante os dois últimos anos, até mesmo na pós-graduação
e em outras faculdades, com a contabilização dos créditos sendo feita por uma disciplina própria de nome
"Projetos Especiais" (Eu mesmo cheguei a cursar uma disciplina da Unicamp e uma da pós da FMUSP durante
o meu tempo nessa parte do curso).

Enfim, após o meu ciclo básico que se situou a maior parte fora da pandemia, me decidi por seguir na área da
Biologia Celular, pesquisando sobre o papel da proteína Prion no contexto dos Glioblastomas. Entretanto, a 
maior parte do meu ciclo básico se passou em meio a pandemia então a ideia inicial de se desenvolver um projeto
mais voltado a wet lab não foi possível e no fim das contas assumi um trabalho mais voltado a bioinformática em 
análises de proteômica.

A proteômica em si gerou alguns resultados muito interessantes, que ao serem combinados com uma pesquisa de 
transcriptômica de outra colega do laboratório, levou a um artigo interessante sobre a relação da proteína
prion com a dinâmica de vesículas no contexto do glioblastoma, tudo isso a partir de uma exploração intensa
de dados públicos disponibilizados em alguns artigos. O artigo pode ser acessado [Clicando Aqui](https://link.springer.com/article/10.1186/s12885-024-11914-6), caso seja do seu interesse dar uma conferida melhor sobre o assunto.

Além disso, no contexto das publicações também colaborei com o grupo na escrita de duas revisões da literatura
sobre tópicos secreção de proteínas em tumores cerebrais([Disponível aqui](https://www.frontiersin.org/journals/cell-and-developmental-biology/articles/10.3389/fcell.2022.907423/full)) e sobre mecanismos de transição epitélio mesenquima e 
autofagia associadas a via da WNT ([Disponível aqui](https://www.frontiersin.org/journals/oncology/articles/10.3389/fonc.2020.597743/full)).

Com a flexibilização do isolamento ainda cheguei a retornar as atividades de Laboratório, mas o meu ciclo
avançado já estava mais direcionado aos estudos das ferramentas computacionais utilizadas, estatística e
machine learning. Houve um considerável distanciamento das biológicas em direção ao Instituto de Matemática
e estatística. Esse distanciamento me levou a repensar a minha trajetória na vida acadêmica e contribuiram
para o retorno a engenharia em 2022.
