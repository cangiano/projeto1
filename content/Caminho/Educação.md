---
title: "Parte III - Engenharia Mecatrônica, agora vai!"
date: 2021-09-18T16:59:34-03:00
draft: false
---

### Resumo bom, bonito e barato
Ingressei no curso de Engenharia Mecatrônica da Escola Politécnica da Universidade
de São Paulo em fevereiro de 2018. Após um semestre que foi muito bem encaminhado,
ignorando-se alguns trancos e barrancos PCC3103 - Geometria e Representação Gráficas
(catapultas no NX não são o meu forte), transferi para o curso de Ciências Moleculares (2018-2022).

Após 4 anos (2 últimos passados na pandemia) no segundo semestre de 2022, eu retornei a minha graduação
original de 2018, que permaneceu trancada ao longo do período do outro curso. Após o pedido de várias
equivalências em disciplinas do ciclo básico foi iniciada uma nova jornada.

Dai para frente foi construído um caminho que me levou a ganhar uma competição numa disciplina de introdução ao
projeto mecânico (dois servo motores e empinadas de pura adrenalina), algumas infinitas listas de
eletrônica, laboratórios e outros quase infinitos relatórios, várias provas e pouco sono. E um site de gerenciamento
de estoque ainda em construção.




### Introdução, Digressões e Confusões
Tudo começou com uma decisão conturbada acerca da escolha de curso
a ser seguido no período pós ensino médio. Esse processo de decisão
limitado a um intervalo de tão pouco tempo acaba por dificultar a
escolhas futuras e ofuscar uma visão crítica sobre o que é uma
faculdade além daquele mini-mundo do ensino médio.

A primeira do autor que vos fala foi cursar essa tal Engenharia Mecatrônica
numa visão distorcida que sequer imaginava o tanto de mecânica que seria
envolvida em comparação a parte de eletrônica. Bem, eventualmente esse 
equívoco foi notado e uma tentativa frustrada de entregar umas questões
em branco na segunda fase da Fuvest não foi bem sucedida. Alegria Alegria.

Mas, note, naquele momento o autor tinha a intenção de cursar especificamente
engenharia? Não, não era a ideia principal, a ideia era transferir para um
tal de Ciências Moleculares. Pois bem, a continuação desse bloco de história
pode ser conferida no bloco "Parte II - Ciências Moleculares? O que é isso?"
caso o leitor não tenha optado por uma leitura sequencial. Complicado transitar
em tanto texto que poderia ser um lorem ypsulum epslon Nikola Tesla bibidi babidi
du, ainda mais após várias técnicas de speedrun de leitura de artigos seguindo
resumo, conclusão, resultados, materiais e métodos tipo tentar remontar os 
capítulos de O Jogo da Amarelinha do Cortázar.

No fim, essa página será em grande parte uma digressão pois ela ainda está constantemente
sendo atualizada com o passar do tempo e eventualmente um projeto de pesquisa será
adicionado aqui (o bom filho a casa torna, só que não há filho e a casa agora é outra),
permitindo assim caracterizar isso como uma página em constante transformação.