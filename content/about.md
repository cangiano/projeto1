---
title: "Sobre o propósito desse site"
date: 2021-09-18T16:59:34-03:00
draft: false
---

Esta página talvez devesse conter informações sobre a minha humilde pessoa.
Sou um estudante da Engenharia Mecatrônica no meio de um sexto semestre um 
tanto conturbado mas nem tanto assim.

Esse site estático foi desenvolvido para a disciplina PMR3304 - Sistemas de Informação
para uma primeira entrega da disciplina e tem como intuito aplicar alguns conhecimentos
vistos em aula e aplicações de desenvolvimento rápido com o auxílio do HUGO.

Informações reais (ou quase totalmente reais) sobre a minha humilde pessoa poderam ser
vistas melhor na aba Caminhos.

Além disso, caso seja de interesse conferir um pouco melhor sobre mim, os links abaixo
também serão de interesse:

- [Currículo Lattes](https://lattes.cnpq.br/2719247477296702)
- [Linkedin](https://linkedin.com/in/giovanni-cangiano-a53016193)
- [Fapesp](https://bv.fapesp.br/pt/pesquisador/711288/giovanni-cangiano/)
